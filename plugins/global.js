import Vue from 'vue'

const VueVideoPlayer = require('vue-video-player/dist/ssr')
Vue.use(VueVideoPlayer)

import appLogo from '@/components/global/appLogo';
import contact from '@/components/global/contact';
import otherYachts from '@/components/global/otherYachts';

Vue.component('app-logo', appLogo);
Vue.component('contact', contact);
Vue.component('other-yachts', otherYachts);