import Vue from "vue";
import vuexI18n from "vuex-i18n/dist/vuex-i18n.umd.js";
import toEnglish from "../translations/toEnglish";
import toSpanish from "./../translations/toSpanish";
import toGerman from "./../translations/toGerman";

export default ({ store }) => {
    Vue.use(
        vuexI18n.plugin,
        store,
        {
            onTranslationNotFound: function (locale, key) {
                console.warn(`vuex-i18n :: Key '${key}' not found for locale '${locale}'`)
            }
        }
    );

    // register the locales
    Vue.i18n.add('en', toEnglish);
    Vue.i18n.add('de', toGerman);
    Vue.i18n.add('es', toSpanish);

    // Set the start locale to use
    Vue.i18n.set('de');
    Vue.i18n.fallback('en');
}