import Vuex from "vuex";

/*
export const state = () => ({
    currentLanguage: ''
});

export const mutations = {
    changeLang: (state, response) => {
        if(response) {
            state.currentLanguage = response;
            Vue.i18n.set(response);
            console.log(response);
        }
    }
};
*/

const store = () => {
    return new Vuex.Store({
        state: () => ({
            currentLanguage: ''
        }),
        mutations: {
            changeLang(state, response) {
                if (response) {
                    console.log(this);
                    state.currentLanguage = response;
                    this.$i18n.set(response);
                }
            }
        }
    })
};

function setBaseLang() {
    if (isHMR) return;

    console.log('store current lang is: ', store.state.currentLanguage, 'and browser lang is: ', window.navigator.language);

    console.log(window.navigator.language);

    if(store.state.currentLanguage === '') {
        if(window.navigator.language === 'pl' || window.navigator.language === 'pl-PL') {
            store.commit('changeLang', 'pl');
            console.log('set pl');
        }
        if(window.navigator.language === 'en' || window.navigator.language === 'en-GB' || window.navigator.language === 'en-US') {
            store.commit('changeLang', 'en');
        }
    } else {
        store.commit('changeLang', store.state.currentLanguage);
    }

    if(window.location.href.indexOf('.de') !== -1) {
        store.commit('changeLang', 'de');
    }
    if(window.location.href.indexOf('.co.uk') !== -1) {
        store.commit('changeLang', 'en');
    }
}

//setBaseLang();

export default store;