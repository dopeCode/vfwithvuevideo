const pkg = require('./package');

module.exports = {
    mode: 'universal',

    /*
    ** Headers of the page
    */
    head: {
        title: pkg.name,
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: pkg.description }
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
        ]
    },

    /*
    ** Customize the progress-bar color
    */
    loading: { color: '#fff' },

    /*
    ** Global CSS
    */
    css: [
        'video.js/dist/video-js.css',
        '@/assets/style.css'
    ],

    /*
    ** Plugins to load before mounting the App
    */
    plugins: [
        {
            src: '@/plugins/global.js',
            ssr: false
        },
        {
            src: '@/plugins/localStorage.js',
            ssr: false
        },
        {
            src: '@/plugins/i18n.js',
            ssr: false
        },
        {
            src: '~/node_modules/vue-flux',
            ssr: false
        }
    ],

    /*
    ** Nuxt.js modules
    */
    modules: [
        'bootstrap-vue/nuxt'
    ],

    /*
    ** Build configuration
    */
    build: {
        /*
        ** You can extend webpack config here
        */
        vendor: ['vue-flux'],
        extend(config, ctx) {

        }
    }
}
